# Turkish translation for ubuntu-docviewer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 07:09+0000\n"
"PO-Revision-Date: 2024-05-29 17:23+0000\n"
"Last-Translator: Ali Beyaz <alipolatbeyaz@gmail.com>\n"
"Language-Team: Turkish <https://hosted.weblate.org/projects/lomiri/lomiri-"
"docviewer-app/tr/>\n"
"Language: tr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 5.6-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:49+0000\n"

#: lomiri-docviewer-app.desktop.in:7
msgid "/usr/share/lomiri-docviewer-app/docviewer-app.png"
msgstr "/usr/share/lomiri-docviewer-app/docviewer-app.png"

#: lomiri-docviewer-app.desktop.in:8
msgid "Document Viewer"
msgstr "Belge Görüntüleyici"

#: lomiri-docviewer-app.desktop.in:9
msgid "documents;viewer;pdf;reader;"
msgstr "belgeler;görüntüleyici;pdf;okuyucu;"

#: src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Sağlanan argümanlardan bazıları geçerli değil."

#: src/app/qml/common/CommandLineProxy.qml:58
msgid "Open lomiri-docviewer-app displaying the selected file"
msgstr "Seçilen dosyayı görüntüleyen lomiri-docviewer-app'i açın"

#: src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Tam ekran çalıştır"

#: src/app/qml/common/CommandLineProxy.qml:71
msgid "Open lomiri-docviewer-app in pick mode. Used for tests only."
msgstr ""
"Lomiri-docviewer-app'i seçim modunda açın. Yalnızca deneme amaçlı kullanılır."

#: src/app/qml/common/CommandLineProxy.qml:77
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running lomiri-docviewer-app"
msgstr ""
"~/Documents yerine verilen klasördeki belgeleri gösterin.\n"
"Lomiri-docviewer-app'i çalıştırmadan önce yol mevcut olmalıdır"

#: src/app/qml/common/DetailsPage.qml:29
#: src/app/qml/loView/LOViewDefaultHeader.qml:93
#: src/app/qml/pdfView/PdfView.qml:317
#: src/app/qml/textView/TextViewDefaultHeader.qml:55
msgid "Details"
msgstr "Ayrıntılar"

#: src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Dosya"

#: src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Konum"

#: src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Boyut"

#: src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Oluşturulma"

#: src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Son değiştirilme"

#: src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "MIME türü"

#: src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Hata"

#: src/app/qml/common/ErrorDialog.qml:26
#: src/app/qml/common/PickImportedDialog.qml:54
#: src/app/qml/common/RejectedImportDialog.qml:38
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:33
#: src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Kapat"

#: src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Birden çok belge içe aktarılmış"

#: src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Hangisinin açacağını seçin:"

#: src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Dosya desteklenmiyor"
msgstr[1] "Dosyalar desteklenmiyor"

#: src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "Belge içe aktarılmadı:"
msgstr[1] "Belgeler içe aktarılamadı:"

#: src/app/qml/common/TextualButtonStyle.qml:48
#: src/app/qml/common/TextualButtonStyle.qml:51
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:54
msgid "Pick"
msgstr "Seç"

#: src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Bilinmeyen dosya türü"

#: src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"Bu dosya desteklenmiyor. \n"
"Düz metin olarak açmak ister misiniz?"

#: src/app/qml/common/UnknownTypeDialog.qml:38
#: src/app/qml/documentPage/DeleteFileDialog.qml:55
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:34
#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:33
#: src/app/qml/loView/LOViewGotoDialog.qml:55
#: src/app/qml/pdfView/DocumentLockedDialog.qml:63
#: src/app/qml/pdfView/OpenLinkDialog.qml:39
#: src/app/qml/pdfView/PdfView.qml:261
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "İptal"

#: src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Evet"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 kB"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 bayt"

#: src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Dosyayı sil"

#: src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "%1 dosyayı sil"
msgstr[1] "%1 dosyayı sil"

#: src/app/qml/documentPage/DeleteFileDialog.qml:41
#: src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Bu dosyayı kalıcı olarak silmek istediğinizden emin misiniz?"
msgstr[1] "Bu dosyaları kalıcı olarak silmek istediğinizden emin misiniz?"

#: src/app/qml/documentPage/DeleteFileDialog.qml:61
#: src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:58
msgid "Delete"
msgstr "Sil"

#: src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Paylaş"

#: src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "Belge bulunamadı"

#: src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""
"Cihazınızı herhangi bir bilgisayara bağlayın ve dosyaları sadece Belgeler "
"klasörüne sürükleyin ya da belgeleri içeren çıkarılabilir medyayı "
"yerleştirin."

#: src/app/qml/documentPage/DocumentListDelegate.qml:75
msgid "SD card"
msgstr "SD kartı"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:100
#, qt-format
msgid "Today, %1"
msgstr "Bugün, %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:105
#, qt-format
msgid "Yesterday, %1"
msgstr "Dün, %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:112
#: src/app/qml/documentPage/DocumentListDelegate.qml:131
msgid "yyyy/MM/dd hh:mm"
msgstr "dd/MM/yyyy hh:mm"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:125
msgid "dddd, hh:mm"
msgstr "hh:mm dddd"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:27
msgid "Documents"
msgstr "Belgeler"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:32
msgid "Search…"
msgstr "Ara…"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:39
msgid "Sorting settings…"
msgstr "Sıralama ayarları…"

#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:56
msgid "search in documents..."
msgstr "belgelerde arama…"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select None"
msgstr "Hiçbirini seçme"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select All"
msgstr "Tümünü Seç"

#: src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "Eşleşen bir doküman bulunamadı"

#: src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr ""
"Lütfen sorgunuzun hatalı yazılmış olmadığından ve / veya farklı bir sorgu "
"denediğinden emin olun."

#: src/app/qml/documentPage/SectionHeader.qml:30
msgid "Today"
msgstr "Bugün"

#: src/app/qml/documentPage/SectionHeader.qml:33
msgid "Yesterday"
msgstr "Dün"

#: src/app/qml/documentPage/SectionHeader.qml:36
msgid "Earlier this week"
msgstr "Bu haftanın başlarında"

#: src/app/qml/documentPage/SectionHeader.qml:39
msgid "Earlier this month"
msgstr "Bu ay'ın başlarında"

#: src/app/qml/documentPage/SectionHeader.qml:41
msgid "Even earlier…"
msgstr "Daha da eski…"

#: src/app/qml/documentPage/SharePage.qml:28
msgid "Share to"
msgstr "Paylaş"

#: src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Sıralama ayarları"

#: src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Tarihe göre sırala (Önce en yenisi)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "İsme göre sırala (A-Z)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Boyuta göre sıralama (küçükten büyüğe)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "Ters sıra"

#: src/app/qml/loView/LOViewDefaultHeader.qml:41
#: src/app/qml/textView/TextView.qml:49
msgid "Loading..."
msgstr "Yükleniyor..."

#: src/app/qml/loView/LOViewDefaultHeader.qml:45
msgid "LibreOffice text document"
msgstr "LibreOffice metin belgesi"

#: src/app/qml/loView/LOViewDefaultHeader.qml:47
msgid "LibreOffice spread sheet"
msgstr "LibreOffice elektronik tablosu"

#: src/app/qml/loView/LOViewDefaultHeader.qml:49
msgid "LibreOffice presentation"
msgstr "LibreOffice sunumu"

#: src/app/qml/loView/LOViewDefaultHeader.qml:51
msgid "LibreOffice Draw document"
msgstr "LibreOffice Çizim belgesi"

#: src/app/qml/loView/LOViewDefaultHeader.qml:53
msgid "Unknown LibreOffice document"
msgstr "Bilinmeyen LibreOffice belgesi"

#: src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "Unknown document type"
msgstr "Bilinmeyen belge türü"

#: src/app/qml/loView/LOViewDefaultHeader.qml:72
msgid "Go to position…"
msgstr "Pozisyona git…"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Disable night mode"
msgstr "Gece Modunu devre dışı bırak"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Enable night mode"
msgstr "Gece modunu aktifleştir"

#: src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Pozisyona git"

#: src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "%1 ila %100 arasında bir konum seçin"

#: src/app/qml/loView/LOViewGotoDialog.qml:62
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "GİT!"

#: src/app/qml/loView/LOViewPage.qml:173
msgid "LibreOffice binaries not found."
msgstr "LibreOffice gerekli dosyaları bulunamadı."

#: src/app/qml/loView/LOViewPage.qml:176
msgid "Error while loading LibreOffice."
msgstr "LibreOffice yüklenirken hata oluştu."

#: src/app/qml/loView/LOViewPage.qml:179
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"Belge yüklenmedi. \n"
"İstenen belge bozuk veya bir parola ile korunmuş olabilir."

#: src/app/qml/loView/LOViewPage.qml:231
msgid "This sheet has no content."
msgstr "Bu sayfanın içeriği yok."

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "LibreOfficeKit tarafından desteklenmektedir"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "Excel%1"

#: src/app/qml/loView/ZoomSelector.qml:122
#: src/app/qml/pdfView/ZoomSelector.qml:119
msgid "Fit width"
msgstr "Genişlet"

#: src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Yüksekliğe Sığdır"

#: src/app/qml/loView/ZoomSelector.qml:124
#: src/app/qml/pdfView/ZoomSelector.qml:121
msgid "Automatic"
msgstr "Otomatik"

#: src/app/qml/lomiri-docviewer-app.qml:134
msgid "File does not exist."
msgstr "Dosya bulunmuyor."

#: src/app/qml/pdfView/DocumentLockedDialog.qml:25
msgid "Document is locked"
msgstr "Belge kilitli"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:26
msgid "Please insert a password in order to unlock this document"
msgstr "Bu belgenin kilidini açmak için lütfen bir şifre girin"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:50
msgid "Entered password is not valid"
msgstr "Girilen şifre geçersiz"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:69
msgid "Unlock"
msgstr "Kilidini aç"

#: src/app/qml/pdfView/LinkHint.qml:36
#, qt-format
msgid "Open link externally: %1"
msgstr "Bağlantıyı harici olarak aç: %1"

#: src/app/qml/pdfView/LinkHint.qml:37
#: src/app/qml/pdfView/OpenLinkDialog.qml:28
#, qt-format
msgid "Go to page %1"
msgstr "%1 sayfasına git"

#: src/app/qml/pdfView/OpenLinkDialog.qml:28
msgid "Open link externally"
msgstr "Bağlantıyı harici olarak aç"

#: src/app/qml/pdfView/OpenLinkDialog.qml:29
msgid "Are you sure?"
msgstr "Emin misin?"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Open"
msgstr "Aç"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Go to destination"
msgstr "Hedefe git"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: src/app/qml/pdfView/PdfContentsPage.qml:31
#: src/app/qml/pdfView/PdfView.qml:230
msgid "Contents"
msgstr "İçindekiler"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: src/app/qml/pdfView/PdfPresentation.qml:51
#: src/app/qml/pdfView/PdfView.qml:49
#, qt-format
msgid "Page %1 of %2"
msgstr "Sayfa %1/%2"

#: src/app/qml/pdfView/PdfView.qml:284
msgid "Search"
msgstr "Ara"

#: src/app/qml/pdfView/PdfView.qml:294
msgid "Go to page..."
msgstr "Sayfaya git..."

#: src/app/qml/pdfView/PdfView.qml:303
msgid "Presentation"
msgstr "Sunum"

#: src/app/qml/pdfView/PdfView.qml:324
msgid "Rotate 90° right"
msgstr "90° sağa döndür"

#: src/app/qml/pdfView/PdfView.qml:341
msgid "Rotate 90° left"
msgstr "90° sola döndür"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Sayfaya git"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "1 ile %1 arası bir sayfa seçin"

#: src/app/qml/pdfView/ZoomSelector.qml:120
msgid "Fit page"
msgstr "Sayfayı sığdır"

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: src/plugin/file-qml-plugin/docviewerutils.cpp:119
#, qt-format
msgid "copy %1"
msgstr "kopyala %1"

#~ msgid "Search..."
#~ msgstr "Ara..."

#~ msgid "Sorting settings..."
#~ msgstr "Ayarların sıralanması…"

#~ msgid "Switch to single column list"
#~ msgstr "Tek sütunlu listeye geç"

#~ msgid "Switch to grid"
#~ msgstr "Izgaraya geç"

#~ msgid "Back"
#~ msgstr "Geri"

#~ msgid "Go to position..."
#~ msgstr "Konuma git..."

#~ msgid "File does not exist"
#~ msgstr "Dosya mevcut değil"

#~ msgid "No document found"
#~ msgstr "Dosya bulunamadı"
