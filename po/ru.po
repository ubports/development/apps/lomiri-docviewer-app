# Russian translation for ubuntu-docviewer-app
# Copyright (c) 2015 Rosetta Contributors and Canonical Ltd 2015
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 07:09+0000\n"
"PO-Revision-Date: 2023-01-16 00:59+0000\n"
"Last-Translator: Sergii Horichenko <m@sgg.im>\n"
"Language-Team: Russian <https://hosted.weblate.org/projects/lomiri/lomiri-"
"docviewer-app/ru/>\n"
"Language: ru\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 4.15.1-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:49+0000\n"

#: lomiri-docviewer-app.desktop.in:7
msgid "/usr/share/lomiri-docviewer-app/docviewer-app.png"
msgstr "/usr/share/lomiri-docviewer-app/docviewer-app.png"

#: lomiri-docviewer-app.desktop.in:8
msgid "Document Viewer"
msgstr "Программа просмотра документов"

#: lomiri-docviewer-app.desktop.in:9
msgid "documents;viewer;pdf;reader;"
msgstr "документы;просмотр;pdf;чтение;"

#: src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Некоторые из приведенных аргументов некорректны."

#: src/app/qml/common/CommandLineProxy.qml:58
msgid "Open lomiri-docviewer-app displaying the selected file"
msgstr "Открыть выбранный файл в приложении для просмотра документов"

#: src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Запустить в полноэкранном режиме"

#: src/app/qml/common/CommandLineProxy.qml:71
msgid "Open lomiri-docviewer-app in pick mode. Used for tests only."
msgstr ""
"Открыть приложение для просмотра документов в режиме выбора. Используется "
"только для тестирования."

#: src/app/qml/common/CommandLineProxy.qml:77
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running lomiri-docviewer-app"
msgstr ""
"Отображать документы из выбранной папки, вместо ~/Documents.\n"
"Выбранная папка должна быть создана до запуска приложения просмотра "
"документов"

#: src/app/qml/common/DetailsPage.qml:29
#: src/app/qml/loView/LOViewDefaultHeader.qml:93
#: src/app/qml/pdfView/PdfView.qml:317
#: src/app/qml/textView/TextViewDefaultHeader.qml:55
msgid "Details"
msgstr "Подробности"

#: src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Файл"

#: src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Местоположение"

#: src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Размер"

#: src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Создано"

#: src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Изменён"

#: src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "Тип MIME"

#: src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Ошибка"

#: src/app/qml/common/ErrorDialog.qml:26
#: src/app/qml/common/PickImportedDialog.qml:54
#: src/app/qml/common/RejectedImportDialog.qml:38
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:33
#: src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Закрыть"

#: src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Импортировано несколько документов"

#: src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Выберите документ для открытия:"

#: src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Файл не поддерживается"
msgstr[1] "Файла не поддерживаются"
msgstr[2] "Файлов не поддерживаются"

#: src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "Следующий документ не был импортирован:"
msgstr[1] "Следующие документы не были импортированы:"
msgstr[2] "Следующие документы не были импортированы:"

#: src/app/qml/common/TextualButtonStyle.qml:48
#: src/app/qml/common/TextualButtonStyle.qml:51
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:54
msgid "Pick"
msgstr "Выбрать"

#: src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Неизвестный тип файла"

#: src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"Данный тип файлов не поддерживается.\n"
"Открыть его как обычный текст?"

#: src/app/qml/common/UnknownTypeDialog.qml:38
#: src/app/qml/documentPage/DeleteFileDialog.qml:55
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:34
#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:33
#: src/app/qml/loView/LOViewGotoDialog.qml:55
#: src/app/qml/pdfView/DocumentLockedDialog.qml:63
#: src/app/qml/pdfView/OpenLinkDialog.qml:39
#: src/app/qml/pdfView/PdfView.qml:261
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "Отмена"

#: src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Да"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 Гб"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 Mб"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 Кб"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 байт"

#: src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Удалить файл"

#: src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "Удалить %1 файл"
msgstr[1] "Удалить %1 файла"
msgstr[2] "Удалить %1 файлов"

#: src/app/qml/documentPage/DeleteFileDialog.qml:41
#: src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Вы уверены, что хотите удалить этот файл окончательно?"
msgstr[1] "Вы уверены, что хотите удалить эти файлы окончательно?"
msgstr[2] "Вы уверены, что хотите удалить эти файлы окончательно?"

#: src/app/qml/documentPage/DeleteFileDialog.qml:61
#: src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:58
msgid "Delete"
msgstr "Удалить"

#: src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Поделиться"

#: src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "Не найдено документов"

#: src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""
"Подключите устройство к компьютеру и просто скопируйте файлы в папку "
"Documents либо вставьте внешний носитель, содержащий файлы документов."

#: src/app/qml/documentPage/DocumentListDelegate.qml:75
msgid "SD card"
msgstr "SD-карта"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:100
#, qt-format
msgid "Today, %1"
msgstr "Сегодня, %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:105
#, qt-format
msgid "Yesterday, %1"
msgstr "Вчера, %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:112
#: src/app/qml/documentPage/DocumentListDelegate.qml:131
msgid "yyyy/MM/dd hh:mm"
msgstr "dd.MM.yyyy hh:mm"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:125
msgid "dddd, hh:mm"
msgstr "dddd, hh:mm"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:27
msgid "Documents"
msgstr "Документы"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:32
msgid "Search…"
msgstr "Поиск…"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:39
msgid "Sorting settings…"
msgstr "Настройки сортировки…"

#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:56
msgid "search in documents..."
msgstr "поиск в документах…"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select None"
msgstr "Ничего не выбрано"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select All"
msgstr "Выбрать всё"

#: src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "Подходящие документы не найдены"

#: src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr ""
"Проверьте запрос на наличие ошибок и/или попробуйте выполнить другой запрос."

#: src/app/qml/documentPage/SectionHeader.qml:30
msgid "Today"
msgstr "Сегодня"

#: src/app/qml/documentPage/SectionHeader.qml:33
msgid "Yesterday"
msgstr "Вчера"

#: src/app/qml/documentPage/SectionHeader.qml:36
msgid "Earlier this week"
msgstr "Ранее на этой неделе"

#: src/app/qml/documentPage/SectionHeader.qml:39
msgid "Earlier this month"
msgstr "Ранее в этом месяце"

#: src/app/qml/documentPage/SectionHeader.qml:41
msgid "Even earlier…"
msgstr "Ещё раньше…"

#: src/app/qml/documentPage/SharePage.qml:28
msgid "Share to"
msgstr "Поделиться с"

#: src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Настройки сортировки"

#: src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Сортировать по дате (сначала более поздние)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "Сортировать по имени (A-Z)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Сортировать по размеру (сначала меньшие)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "Обратный порядок"

#: src/app/qml/loView/LOViewDefaultHeader.qml:41
#: src/app/qml/textView/TextView.qml:49
msgid "Loading..."
msgstr "Загрузка..."

#: src/app/qml/loView/LOViewDefaultHeader.qml:45
msgid "LibreOffice text document"
msgstr "Текстовый документ LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:47
msgid "LibreOffice spread sheet"
msgstr "Таблица LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:49
msgid "LibreOffice presentation"
msgstr "Презентация LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:51
msgid "LibreOffice Draw document"
msgstr "Документ LibreOffice Draw"

#: src/app/qml/loView/LOViewDefaultHeader.qml:53
msgid "Unknown LibreOffice document"
msgstr "Неизвестный документ LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "Unknown document type"
msgstr "Неизвестный тип документа"

#: src/app/qml/loView/LOViewDefaultHeader.qml:72
msgid "Go to position…"
msgstr "Перейти на позицию…"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Disable night mode"
msgstr "Выключить ночной режим"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Enable night mode"
msgstr "Включить ночной режим"

#: src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Перейти"

#: src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "Выберите место перехода между 1% и 100%"

#: src/app/qml/loView/LOViewGotoDialog.qml:62
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "Перейти!"

#: src/app/qml/loView/LOViewPage.qml:173
msgid "LibreOffice binaries not found."
msgstr "Файлы LibreOffice не найдены."

#: src/app/qml/loView/LOViewPage.qml:176
msgid "Error while loading LibreOffice."
msgstr "Ошибка при загрузке LibreOffice."

#: src/app/qml/loView/LOViewPage.qml:179
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"Не удалось открыть документ.\n"
"Запрошенный документ может быть повреждён или защищён паролем."

#: src/app/qml/loView/LOViewPage.qml:231
msgid "This sheet has no content."
msgstr "На этом листе нет данных."

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "На базе LibreOfficeKit"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "Лист%1"

#: src/app/qml/loView/ZoomSelector.qml:122
#: src/app/qml/pdfView/ZoomSelector.qml:119
msgid "Fit width"
msgstr "По ширине"

#: src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Выровнять по высоте"

#: src/app/qml/loView/ZoomSelector.qml:124
#: src/app/qml/pdfView/ZoomSelector.qml:121
msgid "Automatic"
msgstr "Автоматически"

#: src/app/qml/lomiri-docviewer-app.qml:134
msgid "File does not exist."
msgstr "Файл не существует."

#: src/app/qml/pdfView/DocumentLockedDialog.qml:25
msgid "Document is locked"
msgstr "Документ заблокирован"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:26
msgid "Please insert a password in order to unlock this document"
msgstr "Пожалуйста, для разблокировки документа, введите пароль"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:50
msgid "Entered password is not valid"
msgstr "Введён неправильный пароль"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:69
msgid "Unlock"
msgstr "Разблокировать"

#: src/app/qml/pdfView/LinkHint.qml:36
#, qt-format
msgid "Open link externally: %1"
msgstr "Открыть ссылку вовне: %1"

#: src/app/qml/pdfView/LinkHint.qml:37
#: src/app/qml/pdfView/OpenLinkDialog.qml:28
#, qt-format
msgid "Go to page %1"
msgstr "Перейти на страницу %1"

#: src/app/qml/pdfView/OpenLinkDialog.qml:28
msgid "Open link externally"
msgstr "Открыть ссылку вовне"

#: src/app/qml/pdfView/OpenLinkDialog.qml:29
msgid "Are you sure?"
msgstr "Вы уверены?"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Open"
msgstr "Открыть"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Go to destination"
msgstr "Перейти к назначению"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: src/app/qml/pdfView/PdfContentsPage.qml:31
#: src/app/qml/pdfView/PdfView.qml:230
msgid "Contents"
msgstr "Содержание"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: src/app/qml/pdfView/PdfPresentation.qml:51
#: src/app/qml/pdfView/PdfView.qml:49
#, qt-format
msgid "Page %1 of %2"
msgstr "Стр. %1 из %2"

#: src/app/qml/pdfView/PdfView.qml:284
msgid "Search"
msgstr "Поиск"

#: src/app/qml/pdfView/PdfView.qml:294
msgid "Go to page..."
msgstr "На страницу…"

#: src/app/qml/pdfView/PdfView.qml:303
msgid "Presentation"
msgstr "Презентация"

#: src/app/qml/pdfView/PdfView.qml:324
msgid "Rotate 90° right"
msgstr "Повернуть на 90° вправо"

#: src/app/qml/pdfView/PdfView.qml:341
msgid "Rotate 90° left"
msgstr "Повернуть на 90° влево"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Перейти на страницу"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "Выберите страницу между 1 и %1"

#: src/app/qml/pdfView/ZoomSelector.qml:120
msgid "Fit page"
msgstr "Подогнать под страницу"

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: src/plugin/file-qml-plugin/docviewerutils.cpp:119
#, qt-format
msgid "copy %1"
msgstr "копия %1"

#~ msgid "Search..."
#~ msgstr "Поиск..."

#~ msgid "Sorting settings..."
#~ msgstr "Настройки сортировки…"

#~ msgid "Switch to single column list"
#~ msgstr "Переключиться на список"

#~ msgid "Switch to grid"
#~ msgstr "Переключиться на плитку"

#~ msgid "Back"
#~ msgstr "Назад"

#~ msgid "Go to position..."
#~ msgstr "Перейти к..."

#~ msgid "File does not exist"
#~ msgstr "Файл не существует"

#~ msgid "No document found"
#~ msgstr "Документы отсутствуют"

#~ msgid "Hide table of contents"
#~ msgstr "Скрыть содержание"
