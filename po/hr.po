# Croatian translation for ubuntu-docviewer-app
# Copyright (c) 2016 Rosetta Contributors and Canonical Ltd 2016
# This file is distributed under the same license as the ubuntu-docviewer-app package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: ubuntu-docviewer-app\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 07:09+0000\n"
"PO-Revision-Date: 2025-02-07 15:53+0000\n"
"Last-Translator: Milo Ivir <mail@milotype.de>\n"
"Language-Team: Croatian <https://hosted.weblate.org/projects/lomiri/"
"lomiri-docviewer-app/hr/>\n"
"Language: hr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"X-Generator: Weblate 5.10-dev\n"
"X-Launchpad-Export-Date: 2017-04-05 07:49+0000\n"

#: lomiri-docviewer-app.desktop.in:7
msgid "/usr/share/lomiri-docviewer-app/docviewer-app.png"
msgstr "/usr/share/lomiri-docviewer-app/docviewer-app.png"

#: lomiri-docviewer-app.desktop.in:8
msgid "Document Viewer"
msgstr ""

#: lomiri-docviewer-app.desktop.in:9
msgid "documents;viewer;pdf;reader;"
msgstr ""

#: src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Neki navedeni argumenti nisu ispravni."

#: src/app/qml/common/CommandLineProxy.qml:58
msgid "Open lomiri-docviewer-app displaying the selected file"
msgstr ""

#: src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Pokreni cjeloekranski prikaz"

#: src/app/qml/common/CommandLineProxy.qml:71
msgid "Open lomiri-docviewer-app in pick mode. Used for tests only."
msgstr ""

#: src/app/qml/common/CommandLineProxy.qml:77
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running lomiri-docviewer-app"
msgstr ""

#: src/app/qml/common/DetailsPage.qml:29
#: src/app/qml/loView/LOViewDefaultHeader.qml:93
#: src/app/qml/pdfView/PdfView.qml:317
#: src/app/qml/textView/TextViewDefaultHeader.qml:55
msgid "Details"
msgstr "Detalji"

#: src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Datoteka"

#: src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Lokacija"

#: src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Veličina"

#: src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Stvoreno"

#: src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Zadnja promjena"

#: src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "MIME vrsta"

#: src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Greška"

#: src/app/qml/common/ErrorDialog.qml:26
#: src/app/qml/common/PickImportedDialog.qml:54
#: src/app/qml/common/RejectedImportDialog.qml:38
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:33
#: src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Zatvori"

#: src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Uvezeno je više datoteka"

#: src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Odaberi dokument koji želiš otvoriti:"

#: src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Datoteka nije podržana"
msgstr[1] "Datoteke nisu podržane"
msgstr[2] "Datoteke nisu podržane"

#: src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "Sljedeći dokument nije uvezen:"
msgstr[1] "Sljedeći dokumenti nisu uvezeni:"
msgstr[2] "Sljedeći dokumenti nisu uvezeni:"

#: src/app/qml/common/TextualButtonStyle.qml:48
#: src/app/qml/common/TextualButtonStyle.qml:51
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:54
msgid "Pick"
msgstr "Odaberi"

#: src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Nepoznata vrsta datoteke"

#: src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"Ova datoteka nije podržana.\n"
"Želite li je otvoriti kao neformatiran tekst?"

#: src/app/qml/common/UnknownTypeDialog.qml:38
#: src/app/qml/documentPage/DeleteFileDialog.qml:55
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:34
#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:33
#: src/app/qml/loView/LOViewGotoDialog.qml:55
#: src/app/qml/pdfView/DocumentLockedDialog.qml:63
#: src/app/qml/pdfView/OpenLinkDialog.qml:39
#: src/app/qml/pdfView/PdfView.qml:261
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "Odustani"

#: src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Da"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 GB"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 MB"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 kB"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 bajt"

#: src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Obriši datoteku"

#: src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "Obriši %1 datoteku"
msgstr[1] "Obriši %1 datoteke"
msgstr[2] "Obriši %1 datoteka"

#: src/app/qml/documentPage/DeleteFileDialog.qml:41
#: src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Stvarno želiš trajno izbrisati ovu datoteku?"
msgstr[1] "Stvarno želiš trajno izbrisati ove datoteke?"
msgstr[2] "Stvarno želiš trajno izbrisati ove datoteke?"

#: src/app/qml/documentPage/DeleteFileDialog.qml:61
#: src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:58
msgid "Delete"
msgstr "Obriši"

#: src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Podijeli"

#: src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr ""

#: src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""

#: src/app/qml/documentPage/DocumentListDelegate.qml:75
msgid "SD card"
msgstr "SD kartica"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:100
#, qt-format
msgid "Today, %1"
msgstr "Danas, %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:105
#, qt-format
msgid "Yesterday, %1"
msgstr "Jučer, %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:112
#: src/app/qml/documentPage/DocumentListDelegate.qml:131
msgid "yyyy/MM/dd hh:mm"
msgstr ""

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:125
msgid "dddd, hh:mm"
msgstr ""

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:27
msgid "Documents"
msgstr "Dokumenti"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:32
msgid "Search…"
msgstr "Traži …"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:39
msgid "Sorting settings…"
msgstr ""

#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:56
msgid "search in documents..."
msgstr ""

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select None"
msgstr "Poništi izbor"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select All"
msgstr "Odaberi sve"

#: src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr ""

#: src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr ""

#: src/app/qml/documentPage/SectionHeader.qml:30
msgid "Today"
msgstr "Danas"

#: src/app/qml/documentPage/SectionHeader.qml:33
msgid "Yesterday"
msgstr "Jučer"

#: src/app/qml/documentPage/SectionHeader.qml:36
msgid "Earlier this week"
msgstr "Ranije u ovom tjednu"

#: src/app/qml/documentPage/SectionHeader.qml:39
msgid "Earlier this month"
msgstr "Ranije u ovom mjesecu"

#: src/app/qml/documentPage/SectionHeader.qml:41
msgid "Even earlier…"
msgstr "Još ranije …"

#: src/app/qml/documentPage/SharePage.qml:28
msgid "Share to"
msgstr "Podijeli sa"

#: src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Postavke razvrstavanja"

#: src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Razvrstaj po datumu (najprije najnoviji)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "Razvrstaj po imenu (A-Ž)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Razvrstaj po veličini (najprije najmanje)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "Preokreni redoslijed"

#: src/app/qml/loView/LOViewDefaultHeader.qml:41
#: src/app/qml/textView/TextView.qml:49
msgid "Loading..."
msgstr "Učitavanje..."

#: src/app/qml/loView/LOViewDefaultHeader.qml:45
msgid "LibreOffice text document"
msgstr "LibreOffice tekstualni dokument"

#: src/app/qml/loView/LOViewDefaultHeader.qml:47
msgid "LibreOffice spread sheet"
msgstr "LibreOffice proračunska tablica"

#: src/app/qml/loView/LOViewDefaultHeader.qml:49
msgid "LibreOffice presentation"
msgstr "LibreOffice prezentacija"

#: src/app/qml/loView/LOViewDefaultHeader.qml:51
msgid "LibreOffice Draw document"
msgstr "LibreOffice crtež"

#: src/app/qml/loView/LOViewDefaultHeader.qml:53
msgid "Unknown LibreOffice document"
msgstr "Nepoznat LibreOffice dokument"

#: src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "Unknown document type"
msgstr "Nepoznata vrsta dokumenta"

#: src/app/qml/loView/LOViewDefaultHeader.qml:72
msgid "Go to position…"
msgstr "Idi na poziciju …"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Disable night mode"
msgstr "Deaktiviraj noćni modus"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Enable night mode"
msgstr "Aktiviraj noćni modus"

#: src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Idi na poziciju"

#: src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "Odaberi poziciju između 1% i 100%"

#: src/app/qml/loView/LOViewGotoDialog.qml:62
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "KRENI!"

#: src/app/qml/loView/LOViewPage.qml:173
msgid "LibreOffice binaries not found."
msgstr "Binarne datoteke za LibreOffice nisu pronađene."

#: src/app/qml/loView/LOViewPage.qml:176
msgid "Error while loading LibreOffice."
msgstr "Greška tijekom učitavanja aplikacije LibreOffice."

#: src/app/qml/loView/LOViewPage.qml:179
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"Dokument nije učitan.\n"
"Zatraženi dokument je možda oštećen ili zaštićen lozinkom."

#: src/app/qml/loView/LOViewPage.qml:231
msgid "This sheet has no content."
msgstr "Ovaj list nema sadržaja."

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "Pokreće LibreOfficeKit"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "List%1"

#: src/app/qml/loView/ZoomSelector.qml:122
#: src/app/qml/pdfView/ZoomSelector.qml:119
msgid "Fit width"
msgstr "Prilagodi prema širini"

#: src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Prilagodi prema visini"

#: src/app/qml/loView/ZoomSelector.qml:124
#: src/app/qml/pdfView/ZoomSelector.qml:121
msgid "Automatic"
msgstr "Automatski"

#: src/app/qml/lomiri-docviewer-app.qml:134
msgid "File does not exist."
msgstr "Datoteka ne postoji."

#: src/app/qml/pdfView/DocumentLockedDialog.qml:25
msgid "Document is locked"
msgstr "Dokument ja zaključan"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:26
msgid "Please insert a password in order to unlock this document"
msgstr "Za otključavanje ovog dokumenta upiši lozinku"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:50
msgid "Entered password is not valid"
msgstr "Upisana lozinka nije ispravna"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:69
msgid "Unlock"
msgstr "Otključaj"

#: src/app/qml/pdfView/LinkHint.qml:36
#, qt-format
msgid "Open link externally: %1"
msgstr "Otvori poveznicu eksterno: %1"

#: src/app/qml/pdfView/LinkHint.qml:37
#: src/app/qml/pdfView/OpenLinkDialog.qml:28
#, qt-format
msgid "Go to page %1"
msgstr "Idi na stranicu %1"

#: src/app/qml/pdfView/OpenLinkDialog.qml:28
msgid "Open link externally"
msgstr "Otvori poveznicu eksterno"

#: src/app/qml/pdfView/OpenLinkDialog.qml:29
msgid "Are you sure?"
msgstr "Sigurno?"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Open"
msgstr "Otvori"

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Go to destination"
msgstr "Idi na odredište"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: src/app/qml/pdfView/PdfContentsPage.qml:31
#: src/app/qml/pdfView/PdfView.qml:230
msgid "Contents"
msgstr "Sadržaj"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: src/app/qml/pdfView/PdfPresentation.qml:51
#: src/app/qml/pdfView/PdfView.qml:49
#, qt-format
msgid "Page %1 of %2"
msgstr "Stranica %1 od %2"

#: src/app/qml/pdfView/PdfView.qml:284
msgid "Search"
msgstr "Traži"

#: src/app/qml/pdfView/PdfView.qml:294
msgid "Go to page..."
msgstr "Idi na stranicu …"

#: src/app/qml/pdfView/PdfView.qml:303
msgid "Presentation"
msgstr "Prezentacija"

#: src/app/qml/pdfView/PdfView.qml:324
msgid "Rotate 90° right"
msgstr "Okreni za 90° nadesno"

#: src/app/qml/pdfView/PdfView.qml:341
msgid "Rotate 90° left"
msgstr "Okreni za 90° nalijevo"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Idi na stranicu"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "Odaberi stranicu između 1 i %1"

#: src/app/qml/pdfView/ZoomSelector.qml:120
msgid "Fit page"
msgstr "Prilagodi prema stranici"

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: src/plugin/file-qml-plugin/docviewerutils.cpp:119
#, qt-format
msgid "copy %1"
msgstr "kopija %1"

#~ msgid "Search..."
#~ msgstr "Pretraga..."

#~ msgid "Back"
#~ msgstr "Natrag"
