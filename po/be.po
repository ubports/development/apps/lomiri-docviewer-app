# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Canonical Ltd.
# This file is distributed under the same license as the  package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2023-01-10 07:09+0000\n"
"PO-Revision-Date: 2020-12-08 12:50+0000\n"
"Last-Translator: Zmicer <nashtlumach@gmail.com>\n"
"Language-Team: Belarusian <https://translate.ubports.com/projects/ubports/"
"docviewer-app/be/>\n"
"Language: be\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=n%10==1 && n%100!=11 ? 0 : n%10>=2 && "
"n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Weblate 3.11.3\n"

#: lomiri-docviewer-app.desktop.in:7
msgid "/usr/share/lomiri-docviewer-app/docviewer-app.png"
msgstr ""

#: lomiri-docviewer-app.desktop.in:8
#, fuzzy
#| msgid "Document Viewer"
msgid "Document Viewer"
msgstr "Прагляд дакументаў"

#: lomiri-docviewer-app.desktop.in:9
msgid "documents;viewer;pdf;reader;"
msgstr "дакументы;прагляд;pdf;чытанне;"

#: src/app/qml/common/CommandLineProxy.qml:49
msgid "Some of the provided arguments are not valid."
msgstr "Некаторыя аргументы непрыдатныя."

#: src/app/qml/common/CommandLineProxy.qml:58
#, fuzzy
#| msgid "Open ubuntu-docviewer-app displaying the selected file"
msgid "Open lomiri-docviewer-app displaying the selected file"
msgstr "Адкрыць абраны файл у праграме для прагляду дакументаў"

#: src/app/qml/common/CommandLineProxy.qml:65
msgid "Run fullscreen"
msgstr "Запусціць у поўнаэкранным рэжыме"

#: src/app/qml/common/CommandLineProxy.qml:71
#, fuzzy
#| msgid "Open ubuntu-docviewer-app in pick mode. Used for tests only."
msgid "Open lomiri-docviewer-app in pick mode. Used for tests only."
msgstr ""
"Адкрыць праграму ў рэжыме выбару. Выкарыстоўваецца толькі для тэставання."

#: src/app/qml/common/CommandLineProxy.qml:77
#, fuzzy
#| msgid ""
#| "Show documents from the given folder, instead of ~/Documents.\n"
#| "The path must exist prior to running ubuntu-docviewer-app"
msgid ""
"Show documents from the given folder, instead of ~/Documents.\n"
"The path must exist prior to running lomiri-docviewer-app"
msgstr ""
"Паказваць дакументы з абранага каталога замест ~/Documents.\n"
"Каталог неабходна стварыць да запуску праграмы"

#: src/app/qml/common/DetailsPage.qml:29
#: src/app/qml/loView/LOViewDefaultHeader.qml:93
#: src/app/qml/pdfView/PdfView.qml:317
#: src/app/qml/textView/TextViewDefaultHeader.qml:55
msgid "Details"
msgstr "Падрабязнасці"

#: src/app/qml/common/DetailsPage.qml:42
msgid "File"
msgstr "Файл"

#: src/app/qml/common/DetailsPage.qml:47
msgid "Location"
msgstr "Месцазнаходжанне"

#: src/app/qml/common/DetailsPage.qml:52
msgid "Size"
msgstr "Памер"

#: src/app/qml/common/DetailsPage.qml:57
msgid "Created"
msgstr "Створана"

#: src/app/qml/common/DetailsPage.qml:62
msgid "Last modified"
msgstr "Апошняя змена"

#: src/app/qml/common/DetailsPage.qml:69
msgid "MIME type"
msgstr "Тып MIME"

#: src/app/qml/common/ErrorDialog.qml:23
msgid "Error"
msgstr "Памылка"

#: src/app/qml/common/ErrorDialog.qml:26
#: src/app/qml/common/PickImportedDialog.qml:54
#: src/app/qml/common/RejectedImportDialog.qml:38
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:33
#: src/app/qml/documentPage/SortSettingsDialog.qml:53
msgid "Close"
msgstr "Закрыць"

#: src/app/qml/common/PickImportedDialog.qml:29
msgid "Multiple documents imported"
msgstr "Імпартавана некалькі дакументаў"

#: src/app/qml/common/PickImportedDialog.qml:30
msgid "Choose which one to open:"
msgstr "Абярыце дакумент, які патрэбна адкрыць:"

#: src/app/qml/common/RejectedImportDialog.qml:28
msgid "File not supported"
msgid_plural "Files not supported"
msgstr[0] "Файл не падтрымліваецца"
msgstr[1] "Файлы не падтрымліваюцца"
msgstr[2] "Файлы не падтрымліваюцца"

#: src/app/qml/common/RejectedImportDialog.qml:29
msgid "Following document has not been imported:"
msgid_plural "Following documents have not been imported:"
msgstr[0] "Наступны дакумент не быў імпартаваны:"
msgstr[1] "Наступныя дакументы не былі імпартаваныя:"
msgstr[2] "Наступныя дакументы не былі імпартаваныя:"

#: src/app/qml/common/TextualButtonStyle.qml:48
#: src/app/qml/common/TextualButtonStyle.qml:51
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:54
msgid "Pick"
msgstr "Абраць"

#: src/app/qml/common/UnknownTypeDialog.qml:27
msgid "Unknown file type"
msgstr "Невядомы тып файла"

#: src/app/qml/common/UnknownTypeDialog.qml:28
msgid ""
"This file is not supported.\n"
"Do you want to open it as a plain text?"
msgstr ""
"Файлы гэтага тыпу не падтрымліваюцца.\n"
"Адкрыць як просты тэкст?"

#: src/app/qml/common/UnknownTypeDialog.qml:38
#: src/app/qml/documentPage/DeleteFileDialog.qml:55
#: src/app/qml/documentPage/DocumentPagePickModeHeader.qml:34
#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:33
#: src/app/qml/loView/LOViewGotoDialog.qml:55
#: src/app/qml/pdfView/DocumentLockedDialog.qml:63
#: src/app/qml/pdfView/OpenLinkDialog.qml:39
#: src/app/qml/pdfView/PdfView.qml:261
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:51
msgid "Cancel"
msgstr "Скасаваць"

#: src/app/qml/common/UnknownTypeDialog.qml:44
msgid "Yes"
msgstr "Так"

#. TRANSLATORS: %1 is the size of a file, expressed in GB
#: src/app/qml/common/utils.js:22
#, qt-format
msgid "%1 GB"
msgstr "%1 Гб"

#. TRANSLATORS: %1 is the size of a file, expressed in MB
#: src/app/qml/common/utils.js:26
#, qt-format
msgid "%1 MB"
msgstr "%1 Мб"

#. TRANSLATORS: %1 is the size of a file, expressed in kB
#: src/app/qml/common/utils.js:30
#, qt-format
msgid "%1 kB"
msgstr "%1 Кб"

#. TRANSLATORS: %1 is the size of a file, expressed in byte
#: src/app/qml/common/utils.js:33
#, qt-format
msgid "%1 byte"
msgstr "%1 б"

#: src/app/qml/documentPage/DeleteFileDialog.qml:39
msgid "Delete file"
msgstr "Выдаліць файл"

#: src/app/qml/documentPage/DeleteFileDialog.qml:40
#, qt-format
msgid "Delete %1 file"
msgid_plural "Delete %1 files"
msgstr[0] "Выдаліць %1 файл"
msgstr[1] "Выдаліць %1 файлы"
msgstr[2] "Выдаліць %1 файлаў"

#: src/app/qml/documentPage/DeleteFileDialog.qml:41
#: src/app/qml/documentPage/DeleteFileDialog.qml:42
msgid "Are you sure you want to permanently delete this file?"
msgid_plural "Are you sure you want to permanently delete these files?"
msgstr[0] "Сапраўды хочаце выдаліць гэты файл?"
msgstr[1] "Сапраўды хочаце выдаліць гэтыя файлы?"
msgstr[2] "Сапраўды хочаце выдаліць гэтыя файлы?"

#: src/app/qml/documentPage/DeleteFileDialog.qml:61
#: src/app/qml/documentPage/DocumentDelegateActions.qml:25
#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:58
msgid "Delete"
msgstr "Выдаліць"

#: src/app/qml/documentPage/DocumentDelegateActions.qml:44
msgid "Share"
msgstr "Падзяліцца"

#: src/app/qml/documentPage/DocumentEmptyState.qml:27
msgid "No documents found"
msgstr "Дакументаў не знойдзена"

#: src/app/qml/documentPage/DocumentEmptyState.qml:28
msgid ""
"Connect your device to any computer and simply drag files to the Documents "
"folder or insert removable media containing documents."
msgstr ""
"Падлучыце прыладу да камп'ютара і скапіюйце файлы ў каталог дакументаў альбо "
"ўстаўце носьбіт з дакументамі."

#: src/app/qml/documentPage/DocumentListDelegate.qml:75
msgid "SD card"
msgstr "SD-картка"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:100
#, qt-format
msgid "Today, %1"
msgstr "Сёння, %1"

#. TRANSLATORS: %1 refers to a time formatted as Locale.ShortFormat (e.g. hh:mm). It depends on system settings.
#. http://qt-project.org/doc/qt-4.8/qlocale.html#FormatType-enum
#: src/app/qml/documentPage/DocumentListDelegate.qml:105
#, qt-format
msgid "Yesterday, %1"
msgstr "Учора, %1"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:112
#: src/app/qml/documentPage/DocumentListDelegate.qml:131
msgid "yyyy/MM/dd hh:mm"
msgstr "dd/MM/yyyy hh:mm"

#. TRANSLATORS: this is a datetime formatting string,
#. see http://qt-project.org/doc/qt-5/qml-qtqml-date.html#details for valid expressions.
#: src/app/qml/documentPage/DocumentListDelegate.qml:125
msgid "dddd, hh:mm"
msgstr "dddd, hh:mm"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:27
msgid "Documents"
msgstr "Дакументы"

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:32
#, fuzzy
#| msgid "Search..."
msgid "Search…"
msgstr "Пошук..."

#: src/app/qml/documentPage/DocumentPageDefaultHeader.qml:39
#, fuzzy
#| msgid "Sorting settings"
msgid "Sorting settings…"
msgstr "Налады сартавання"

#: src/app/qml/documentPage/DocumentPageSearchHeader.qml:56
msgid "search in documents..."
msgstr "пошук у дакументах…"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select None"
msgstr "Нічога не абрана"

#: src/app/qml/documentPage/DocumentPageSelectionModeHeader.qml:46
msgid "Select All"
msgstr "Абраць усе"

#: src/app/qml/documentPage/SearchEmptyState.qml:24
msgid "No matching document found"
msgstr "Адпаведных дакументаў не знойдзена"

#: src/app/qml/documentPage/SearchEmptyState.qml:26
msgid ""
"Please ensure that your query is not misspelled and/or try a different query."
msgstr "Праверце запыт на наяўнасць памылак ці паспрабуйце іншыя параметры."

#: src/app/qml/documentPage/SectionHeader.qml:30
msgid "Today"
msgstr "Сёння"

#: src/app/qml/documentPage/SectionHeader.qml:33
msgid "Yesterday"
msgstr "Учора"

#: src/app/qml/documentPage/SectionHeader.qml:36
msgid "Earlier this week"
msgstr "Раней на гэтым тыдні"

#: src/app/qml/documentPage/SectionHeader.qml:39
msgid "Earlier this month"
msgstr "Раней у гэтым месяцы"

#: src/app/qml/documentPage/SectionHeader.qml:41
#, fuzzy
#| msgid "Even earlier..."
msgid "Even earlier…"
msgstr "Даўней…"

#: src/app/qml/documentPage/SharePage.qml:28
msgid "Share to"
msgstr "Падзяліцца"

#: src/app/qml/documentPage/SortSettingsDialog.qml:26
msgid "Sorting settings"
msgstr "Налады сартавання"

#: src/app/qml/documentPage/SortSettingsDialog.qml:31
msgid "Sort by date (Latest first)"
msgstr "Сартаваць па даце (спачатку старэйшыя)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:32
msgid "Sort by name (A-Z)"
msgstr "Сартаваць па назве (А-Я)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:33
msgid "Sort by size (Smaller first)"
msgstr "Сартаваць па памеры (спачатку меншыя)"

#: src/app/qml/documentPage/SortSettingsDialog.qml:47
msgid "Reverse order"
msgstr "Адваротны парадак"

#: src/app/qml/loView/LOViewDefaultHeader.qml:41
#: src/app/qml/textView/TextView.qml:49
msgid "Loading..."
msgstr "Загрузка..."

#: src/app/qml/loView/LOViewDefaultHeader.qml:45
msgid "LibreOffice text document"
msgstr "Тэкставы дакумент LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:47
msgid "LibreOffice spread sheet"
msgstr "Табліца LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:49
msgid "LibreOffice presentation"
msgstr "Прэзентацыя LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:51
msgid "LibreOffice Draw document"
msgstr "Дакумент LibreOffice Draw"

#: src/app/qml/loView/LOViewDefaultHeader.qml:53
msgid "Unknown LibreOffice document"
msgstr "Невядомы дакумент LibreOffice"

#: src/app/qml/loView/LOViewDefaultHeader.qml:55
msgid "Unknown document type"
msgstr "Невядомы тып дакумента"

#: src/app/qml/loView/LOViewDefaultHeader.qml:72
msgid "Go to position…"
msgstr "Перайсці да…"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Disable night mode"
msgstr "Адключыць начны рэжым"

#: src/app/qml/loView/LOViewDefaultHeader.qml:86
#: src/app/qml/pdfView/PdfView.qml:310
#: src/app/qml/textView/TextViewDefaultHeader.qml:49
msgid "Enable night mode"
msgstr "Уключыць начны рэжым"

#: src/app/qml/loView/LOViewGotoDialog.qml:30
msgid "Go to position"
msgstr "Перайсці да"

#: src/app/qml/loView/LOViewGotoDialog.qml:31
msgid "Choose a position between 1% and 100%"
msgstr "Абярыце пазіцыю паміж 1% і 100%"

#: src/app/qml/loView/LOViewGotoDialog.qml:62
#: src/app/qml/pdfView/PdfViewGotoDialog.qml:58
msgid "GO!"
msgstr "Перайсці!"

#: src/app/qml/loView/LOViewPage.qml:173
msgid "LibreOffice binaries not found."
msgstr "Файлаў LibreOffice не знойдзена."

#: src/app/qml/loView/LOViewPage.qml:176
msgid "Error while loading LibreOffice."
msgstr "Не атрымалася загрузіць LibreOffice."

#: src/app/qml/loView/LOViewPage.qml:179
msgid ""
"Document not loaded.\n"
"The requested document may be corrupt or protected by a password."
msgstr ""
"Не атрымалася загрузіць дакумент.\n"
"Ён можа быць пашкоджаны альбо абаронены паролем."

#: src/app/qml/loView/LOViewPage.qml:231
msgid "This sheet has no content."
msgstr "Гэты аркуш пусты."

#. TRANSLATORS: 'LibreOfficeKit' is the name of the library used by
#. Document Viewer for rendering LibreOffice/MS-Office documents.
#. Ref. https://docs.libreoffice.org/libreofficekit.html
#: src/app/qml/loView/Splashscreen.qml:45
msgid "Powered by LibreOfficeKit"
msgstr "Распрацавана на LibreOfficeKit"

#. TRANSLATORS: Please don't add any space between "Sheet" and "%1".
#. This is the default name for a sheet in LibreOffice.
#: src/app/qml/loView/SpreadsheetSelector.qml:64
#, qt-format
msgid "Sheet%1"
msgstr "Аркуш%1"

#: src/app/qml/loView/ZoomSelector.qml:122
#: src/app/qml/pdfView/ZoomSelector.qml:119
msgid "Fit width"
msgstr "Па шырыні"

#: src/app/qml/loView/ZoomSelector.qml:123
msgid "Fit height"
msgstr "Па вышыні"

#: src/app/qml/loView/ZoomSelector.qml:124
#: src/app/qml/pdfView/ZoomSelector.qml:121
msgid "Automatic"
msgstr "Аўтаматычна"

#: src/app/qml/lomiri-docviewer-app.qml:134
msgid "File does not exist."
msgstr "Файл не існуе."

#: src/app/qml/pdfView/DocumentLockedDialog.qml:25
#, fuzzy
#| msgid "Document Viewer"
msgid "Document is locked"
msgstr "Прагляд дакументаў"

#: src/app/qml/pdfView/DocumentLockedDialog.qml:26
msgid "Please insert a password in order to unlock this document"
msgstr ""

#: src/app/qml/pdfView/DocumentLockedDialog.qml:50
msgid "Entered password is not valid"
msgstr ""

#: src/app/qml/pdfView/DocumentLockedDialog.qml:69
msgid "Unlock"
msgstr ""

#: src/app/qml/pdfView/LinkHint.qml:36
#, qt-format
msgid "Open link externally: %1"
msgstr ""

#: src/app/qml/pdfView/LinkHint.qml:37
#: src/app/qml/pdfView/OpenLinkDialog.qml:28
#, fuzzy, qt-format
#| msgid "Go to page"
msgid "Go to page %1"
msgstr "Перайсці да старонкі"

#: src/app/qml/pdfView/OpenLinkDialog.qml:28
msgid "Open link externally"
msgstr ""

#: src/app/qml/pdfView/OpenLinkDialog.qml:29
msgid "Are you sure?"
msgstr ""

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
msgid "Open"
msgstr ""

#: src/app/qml/pdfView/OpenLinkDialog.qml:45
#, fuzzy
#| msgid "Go to position"
msgid "Go to destination"
msgstr "Перайсці да"

#. TRANSLATORS: "Contents" refers to the "Table of Contents" of a PDF document.
#: src/app/qml/pdfView/PdfContentsPage.qml:31
#: src/app/qml/pdfView/PdfView.qml:230
msgid "Contents"
msgstr "Змесціва"

#. TRANSLATORS: the first argument (%1) refers to the page currently shown on the screen,
#. while the second one (%2) refers to the total pages count.
#: src/app/qml/pdfView/PdfPresentation.qml:51
#: src/app/qml/pdfView/PdfView.qml:49
#, qt-format
msgid "Page %1 of %2"
msgstr "Старонка %1 з %2"

#: src/app/qml/pdfView/PdfView.qml:284
#, fuzzy
#| msgid "Search..."
msgid "Search"
msgstr "Пошук..."

#: src/app/qml/pdfView/PdfView.qml:294
msgid "Go to page..."
msgstr "Перайсці да старонкі…"

#: src/app/qml/pdfView/PdfView.qml:303
msgid "Presentation"
msgstr "Прэзентацыя"

#: src/app/qml/pdfView/PdfView.qml:324
msgid "Rotate 90° right"
msgstr ""

#: src/app/qml/pdfView/PdfView.qml:341
msgid "Rotate 90° left"
msgstr ""

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:26
msgid "Go to page"
msgstr "Перайсці да старонкі"

#: src/app/qml/pdfView/PdfViewGotoDialog.qml:27
#, qt-format
msgid "Choose a page between 1 and %1"
msgstr "Абярыце старонку паміж 1 і %1"

#: src/app/qml/pdfView/ZoomSelector.qml:120
#, fuzzy
#| msgid "Go to page"
msgid "Fit page"
msgstr "Перайсці да старонкі"

#. TRANSLATORS: This string is used for renaming a copied file,
#. when a file with the same name already exists in user's
#. Documents folder.
#.
#. e.g. "Manual_Aquaris_E4.5_ubuntu_EN.pdf" will become
#. "Manual_Aquaris_E4.5_ubuntu_EN (copy 2).pdf"
#.
#. where "2" is given by the argument "%1"
#.
#: src/plugin/file-qml-plugin/docviewerutils.cpp:119
#, qt-format
msgid "copy %1"
msgstr "копія %1"

#~ msgid "Search..."
#~ msgstr "Пошук..."

#~ msgid "Sorting settings..."
#~ msgstr "Налады сартавання..."
