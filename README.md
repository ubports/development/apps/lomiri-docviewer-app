# Lomiri Document Viewer Application

The Lomiri Document Viewer Application is the official document viewer
application for Ubuntu Touch capable of opening many common document file
formats including PDFs and those supported by LibreOffice.

## Building

See README-Developers.md for the details on how to build this project.

## i18n: Translating lomiri-docviewer-app into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-docviewer-app

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
